import dayjs from "dayjs"
import utils from "@pureadmin/utils"
import duration from "dayjs/plugin/duration"
import { green, blue, bold } from "picocolors"
dayjs.extend(duration)

export function viteBuildInfo() {
  let config
  let startTime
  let endTime
  let outDir
  return {
    name: "vite:buildInfo",
    configResolved(resolvedConfig) {
      config = resolvedConfig
      outDir = resolvedConfig.build?.outDir ?? "dist"
    },
    buildStart() {
      console.log(
        bold(
          green(
            `👏欢迎使用${blue(
              "[vue-pure-admin]"
            )}，如果您感觉不错，记得点击后面链接给个star哦💖 https://github.com/pure-admin/vue-pure-admin`
          )
        )
      )
      if (config.command === "build") {
        startTime = dayjs(new Date())
      }
    },
    closeBundle() {
      if (config.command === "build") {
        endTime = dayjs(new Date())
        utils.getPackageSize({
          folder: outDir,
          callback: (size) => {
            console.log(
              bold(
                green(
                  `🎉恭喜打包完成（总用时${dayjs
                    .duration(endTime.diff(startTime))
                    .format("mm分ss秒")}，打包后的大小为${size}）`
                )
              )
            )
          }
        })
      }
    }
  }
}

// 模拟后端动态生成路由
const permissionRouter = {
    path: "/permission",
    meta: {
        title: "权限管理",
        icon: "lollipop",
        rank: 10
    },
    children: [
        {
            path: "/permission/page/index",
            name: "PermissionPage",
            meta: {
                title: "页面权限",
                roles: ["admin", "common"]
            }
        },
        {
            path: "/permission/button/index",
            name: "PermissionButton",
            meta: {
                title: "按钮权限",
                roles: ["admin", "common"],
                auths: ["btn_add", "btn_edit", "btn_delete"]
            }
        }
    ]
}

export default [
    {
        url: "/getAsyncRoutes",
        method: "get",
        response: () => {
            return {
                success: true,
                data: [permissionRouter]
            }
        }
    }
]

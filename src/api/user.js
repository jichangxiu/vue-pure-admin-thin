import {http} from "@/utils/http"

/** 登录 */
export const getLogin = (data) => {
    return http.request("post", "/login", {data})
}

/** 刷新token */
export const refreshTokenApi = (data) => {
    return http.request("post", "/refreshToken", {data})
}
